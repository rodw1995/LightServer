import {save} from '../redis/repositories/lightRepository';

export default class Light {

    constructor(on = false, until = null, check = true) {
        this.on = on;
        this.timeout = null;
        this.until = until;

        if (check && on === true && (until === null || until.getTime() <= Date.now())) {
            this.on = false;
            this.until = null;
        }
    }

    addLightTime(ms) {
        if (this.on) {
            // Add time
            this.until = new Date(this.until.getTime() + ms);
        } else {
            this.on = true;
            this.until = new Date(Date.now() + ms);
        }

        this._setOffTimeout();

        save(this);
    }

    setOff() {
        if (this.on === true) {
            this.on = false;
            this.until = null;

            if (this.timeout !== null) {
                clearTimeout(this.timeout);
                this.timeout = null;
            }

            save(this);
        }
    }

    _setOffTimeout() {
        if (this.timeout !== null) {
            // Clear current timeout
            clearTimeout(this.timeout);
        }

        // Set off timeout
        this.timeout = setTimeout(() => {
            this.setOff();
        }, this.until.getTime() - Date.now());
    }
}