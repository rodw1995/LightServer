let express = require('express');
let bodyParser = require('body-parser');

let app = express();
app.websocket = require('./websocket');

import lightRoute from './routes/light';
import deviceRoute from './routes/device';
import iotRoute from './routes/iot';
import checkoutRoute from './routes/checkout';
import registerListeners from './events/listeners/registerListeners';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', lightRoute);
app.use('/device', deviceRoute);
app.use('/arduino', iotRoute);
app.use('/checkout', checkoutRoute);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    res.json({state: (err.status || 500), message: err.message});
});

// Register the event listeners
registerListeners(app);

module.exports = app;