let socketio = require('socket.io');
import * as deviceSocketRepository from './redis/repositories/deviceSocketRepository';

let io = socketio();

io.on('connection', (socket) => {
    let device_id = socket.handshake.query.device_id;
    if (device_id) {
        deviceSocketRepository.set(device_id, socket.id);

        socket.on('disconnect', () => {
            deviceSocketRepository.remove(device_id);
        });
    }
});

module.exports = io;