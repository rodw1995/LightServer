/**
 * Set the application config
 */
const appConfig = {
    'FREE_TIME_MS': 60 * 1000,

    'FREE_BUY_TIME_SEC': 60,

    'GULDEN_COSTS': '0.001',

    'GULDEN_PRODUCT_DESCRIPTION': 'Buy <b>1</b> hour of light time for MyLight!',
};

/**
 * Load environment variables and export a function to get config values (app and env)
 */

const env = require('dotenv').config();
const parseEnv = require('dotenv-parse-variables');

if (env.error) throw env.error;

const envParsed = parseEnv(env.parsed);

export default (key, defaultValue = null) => {

    // Check app config
    if (appConfig.hasOwnProperty(key)) {
        return appConfig[key];
    }

    // Check .env variables
    if (envParsed.hasOwnProperty(key)) {
        return envParsed[key];
    }

    // Check process.env
    if (process.env.hasOwnProperty(key)) {
        return process.env[key];
    }

    return defaultValue;
};