import client from '../connect';

const KEY_PREFIX = 'DS_';

/**
 * Set the device and socket mapping
 *
 * @param deviceId
 * @param socketId
 */
function set(deviceId, socketId) {
    client.set(KEY_PREFIX + deviceId, socketId);
}

/**
 * Get the socketId by the deviceId
 *
 * @param deviceId
 * @param callback
 */
function getSocketId(deviceId, callback) {
    client.get(KEY_PREFIX + deviceId, (err, reply) => {
        callback(reply);
    });
}

/**
 * Remove the device and socket mapping
 *
 * @param deviceId
 */
function remove(deviceId) {
    client.del(KEY_PREFIX + deviceId);
}

module.exports.set = set;
module.exports.getSocketId = getSocketId;
module.exports.remove = remove;