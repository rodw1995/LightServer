import client from '../connect';

const KEY = 'IOT_TOKEN';

/**
 * Set the token
 *
 * @param token
 */
function set(token) {
    client.set(KEY, token);
}

/**
 * Get the token
 *
 * @param callback
 */
function getToken(callback) {
    client.get(KEY, (err, reply) => {
        callback(reply);
    });
}

/**
 * Remove the token
 */
function remove() {
    client.del(KEY);
}

module.exports.set = set;
module.exports.getToken = getToken;
module.exports.remove = remove;
