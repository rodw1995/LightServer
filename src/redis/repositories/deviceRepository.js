import client from '../connect';
import config from '../../config';
import eventEmitter from '../../events/eventEmitter';

/**
 * Add device to redis database
 *
 * @param deviceId
 */
function add(deviceId) {
    // Save deviceId
    client.set(deviceId, 1, 'EX', config('FREE_BUY_TIME_SEC'));

    // Raise event when key will expire
    setTimeout(() => {
        eventEmitter.emit('deviceExpired', deviceId);
    }, config('FREE_BUY_TIME_SEC') * 1000);
}

/**
 * Check if a device is set in the redis database
 *
 * @param deviceId
 * @param callback
 */
function has(deviceId, callback) {
    client.exists(deviceId, (err, reply) => {
        callback(reply === 1);
    });
}

module.exports.add = add;
module.exports.has = has;