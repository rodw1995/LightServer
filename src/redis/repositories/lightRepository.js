import eventEmitter from '../../events/eventEmitter';
import client from '../connect';
import Light from '../../models/light';

const LIGHT_ON_KEY = 'LIGHT_ON';
const UNTIL_KEY = 'UNTIL';

/**
 * Only one light instance
 * @type {Light}
 */
let lightInstance = null;

/**
 * Get the light model in the callback
 */
function get(callback) {
    if (lightInstance === null) {
        // Retrieve from database
        _get((light) => {
            lightInstance = light;
            callback(lightInstance);
        });
    } else {
        // Return instance
        callback(lightInstance);
    }
}

/**
 * Save the light
 */
function save(light) {
    // Get the old light
    _get((oldLight) => {
        // Save new light
        lightInstance = light;

        // Save in database
        client.set(LIGHT_ON_KEY, light.on ? 1 : 0);
        client.set(UNTIL_KEY, JSON.stringify(light.until));

        // Fire events
        eventEmitter.emit('lightChanged', {
            new: light,
            old: oldLight,
        });
    }, false);
}

/**
 * Get from database
 *
 * @param callback
 * @param check
 * @private
 */
function _get(callback, check = true) {
    client.get(LIGHT_ON_KEY, function(err, reply) {
        if (JSON.parse(reply) === 1) {
            client.get(UNTIL_KEY, function (err, reply) {
                callback(new Light(true, new Date(JSON.parse(reply)), check));
            });
        } else {
            callback(new Light(false));
        }
    });
}

module.exports.get = get;
module.exports.save = save;