import eventEmitter from '../eventEmitter';
import crypto from 'crypto';
import http from 'http';
import config from '../../config';
import {set} from '../../redis/repositories/iotTokenRepository';

export default (io) => {
    eventEmitter.on('lightChanged', (data) => {
        // Emit message over channel
        io.emit('light_changed', {
            light: {
                on: data.new.on,
                until: data.new.until,
            }
        });
    });

    eventEmitter.on('lightChanged', (data) => {
        if (data.new.on !== data.old.on) {
            // Toggle the real light
            let token = crypto.randomBytes(64).toString('hex');
            set(token);

            const req = http.request({
                host: config('IOT_URL'),
                port: config('IOT_PORT'),
                path: '/toggleLight?token=' + token
            }, (res) => {
                res.setEncoding('utf8');

                let rawData = '';
                res.on('data', (chunk) => { rawData += chunk; });

                res.on('end', () => {
                    const result = JSON.parse(rawData);
                    if (result.success) {
                        //console.log('Successfully toggled the real light');
                    } else {
                        console.log('Error toggling real light: ' + result.message);
                    }
                });
            });

            req.on('error', function(e) {
                console.log('ERROR: ' + e.message);
            });

            req.end();
        }
    });
};