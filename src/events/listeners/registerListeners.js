import onLightChange from './onLightChange';
import onDeviceExpired from './onDeviceExpired';

export default (app) => {

    onLightChange(app.websocket);
    onDeviceExpired(app.websocket);

};