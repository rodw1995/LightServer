import eventEmitter from '../eventEmitter';
import * as deviceSocketRepository from '../../redis/repositories/deviceSocketRepository';

export default (io) => {
    eventEmitter.on('deviceExpired', function(deviceId) {
        // Emit message over channel to specific socket
        deviceSocketRepository.getSocketId(deviceId, (socketId) => {
            if (socketId !== null) {
                io.to(socketId).emit('can_scan_again', deviceId);
            }
        });
    });
};