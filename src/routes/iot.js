import express from 'express';
let router = express.Router();

import {getToken} from '../redis/repositories/iotTokenRepository';

router.get('/:token', (req, res, next) => {
    // Check token is send
    getToken((token) => {
       res.json({
          result: token === req.params.token,
       });
    });
});

export default router;
