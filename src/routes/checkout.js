import express from 'express';
let router = express.Router();

import request from 'request';
import crypto from 'crypto';

import config from '../config';

/**
 * Get a gulden checkout url
 */
router.get('/url', (req, res, next) => {

    const guldenCheckoutHost = 'http://checkout.rodw.nl';

    // Get checkout token
    request.get({
        url: guldenCheckoutHost + '/api/merchant/' + config('GULDEN_CHECKOUT_PUBLIC_KEY') + '/' + config.gulden_costs + '/' +
            config('GULDEN_PRODUCT_DESCRIPTION'),
        json: true,
    }, (err, response, body) => {
        if (body.success) {

            const hmac = crypto.createHmac('sha256', config('GULDEN_CHECKOUT_PRIVATE_KEY'));
            hmac.update(body.token);
            const hash = hmac.digest('hex');

            res.json({
                success: true,
                url: guldenCheckoutHost + '/checkout/' + body.id + '/' + hash,
            });
        } else {

        }
    });
});

router.post('/handled', (req, res, next) => {
    console.log('handled called');
});

export default router;