let express = require('express');
let router = express.Router();

import crypto from 'crypto-js';
import config from '../config';
import * as lightRepository from '../redis/repositories/lightRepository';
import * as deviceRepository from '../redis/repositories/deviceRepository';

router.get('/', (req, res, next) => {
    lightRepository.get(function(light) {
        res.json({state: (light.on ? 'ON' : 'OFF'), until: light.until});
    });
});

router.post('/free', (req, res, next) => {
    // Validate token
    if (crypto.AES.decrypt(req.body.token, config('SHARED_SECRET')).toString(crypto.enc.Utf8) === config('BUY_TOKEN')) {
        // Check device can buy free time
        deviceRepository.has(req.body.device_id, (has) => {
           if (has === true) {
               res.json({state: 'error', 'message': 'Already bought free time this hour'});
           } else {
               lightRepository.get((light) => {
                   // Save new light state
                   light.addLightTime(config('FREE_TIME_MS'));

                   // Add device to repository
                   deviceRepository.add(req.body.device_id);

                   res.json({state: 'success'});
               });
           }
        });
    } else {
        res.json({state: 'error', message: 'Invalid token'});
    }
});

export default router;