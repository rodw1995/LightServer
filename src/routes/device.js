import express from 'express';
let router = express.Router();

import {has as hasDevice} from '../redis/repositories/deviceRepository';

router.get('/:deviceId', (req, res, next) => {
    hasDevice(req.params.deviceId, (has) => {
        res.json({result: has});
    });
});

export default router;